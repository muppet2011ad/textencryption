alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"," "] # Defines alphabet for shift

def Shift(text):
    newtext = ""
    for char in text: # Iterate over characters
        if char.lower() != char:
            flagUpper = True
        else:
            flagUpper = False # Determine whether we're working with upper or lower case
        char = char.lower()
        if char not in alphabet: # If it's not in the list, we can't caesar shift it
            if flagUpper:
                newtext += char.upper()
            else:
                newtext += char
        else: # If it is,
            if alphabet.index(char) + 6 > len(alphabet)-1: # If it goes over the limit,
                if flagUpper:
                    newtext += alphabet[alphabet.index(char)+6-len(alphabet)].upper() # Wrap around to the lower end of the array
                else:
                    newtext += alphabet[alphabet.index(char)+6-len(alphabet)]
            else: # If it works normally
                if flagUpper:
                    newtext += alphabet[alphabet.index(char)+6].upper() # Shift it normally
                else:
                    newtext += alphabet[alphabet.index(char)+6]
    return newtext

def UnShift(text):
    newtext = "" # This first section is exactly the same as above
    for char in text:
        if char.lower() != char:
            flagUpper = True
        else:
            flagUpper = False
        char = char.lower()
        if char not in alphabet:
            if flagUpper:
                newtext += char.upper()
            else:
                newtext += char
        else:
            if alphabet.index(char) - 6 < 0: # If it wraps under the bounds of the array
                if flagUpper:
                    newtext += alphabet[len(alphabet)+(alphabet.index(char) - 6)].upper() # Go to the end of the array
                else:
                    newtext += alphabet[len(alphabet)+(alphabet.index(char) - 6)]
            else: # Otherwise,
                if flagUpper:
                    newtext += alphabet[alphabet.index(char) - 6].upper() # Shift back as normal
                else:
                    newtext += alphabet[alphabet.index(char) - 6]
    return newtext