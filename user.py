import random, getpass

alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
specialchars = "!£$%^&*(){}[]@~#;:/.,|\`¬123456789 " # Defines constants that we need for characters

class User(object):
    username = ""
    password = "" # Declares attributes for user class

    def __init__(self, username, password = ""):
        self.username = username
        self.password = password # Sets the username and password based on creation
        if not self.password: # If a password has not been set (i.e. this is a new user)
            self.PassWdGen() # Generate them a password
        
    def PassWdGen(self):
        randompass = [] # Create an list to store the random array of chars
        for i in range(12): # Add 12 random letters, both upper and lower case to the password
            if random.randint(0,1):
                randompass.append(random.choice(alphabet))
            else:
                randompass.append(random.choice(alphabet).upper())
        randompass[random.randint(0,11)] = random.choice(specialchars) # Replace one of the characters with a special character
        suggestedpassword = "".join(randompass) # Join the array into a string
        print("Here is a suggested random password:", suggestedpassword) # Suggest the random password
        usesuggseted = input("Do you want to use this password? (y/n) ").lower() # Ask the user if they want to use this password
        if usesuggseted == "y": # If they do
            print("Thank you. Your password has been recorded.")
            self.password = suggestedpassword # Save their password
        else: # Otherwise
            while not self.password: # While the password has not been set
                userattempt = getpass.getpass("Enter a password: ") # Ask for a password
                if self.ValidatePassWd(userattempt): # If it validates
                    secondattempt = getpass.getpass("Please confirm: ") # Ask them to confirm it
                    if secondattempt == userattempt: # If they match
                        self.password = userattempt # Save the password
                        print("Thank you. Your password has been recorded.")
                    else: # Otherwise
                        print("Passwords do not match!")
                        continue # Make them do it again
                else: # If it fails to validate, make them do it again
                    print("Your password is not secure enough. Make sure to include two cases, have more than 8 characters and a special character.")
        self.RecordToFile() # Record the user to file

    def ValidatePassWd(self, password):
        if len(password) < 8: # If the password is too short, fail
            return False
        hasUpper = False
        hasLower = False
        hasSpecial = False
        for char in password: # Iterate over the password
            if char.upper() == char:
                hasUpper = True # Check if it has an uppercase letter
            if char.lower() == char:
                hasLower = True # Check if it has a lowercase letter
            if char in specialchars:
                hasSpecial = True # Check if it has a special character
        if hasUpper & hasLower & hasSpecial: # If it has all of these things,
            return True # Pass
        else: # Otherwise
            return False # Fail

    def RecordToFile(self):
        with open("user.txt", "w") as userfile: # Open the file
            userfile.write(self.username+"\n") 
            userfile.write(self.password+"\n") # Write the user data to it

def LoadUserFromFile():
    with open("user.txt", "r") as userfile: # Open the file
        username = userfile.readline().replace("\n","")
        password = userfile.readline().replace("\n","") # Read user data
        return User(username, password) # Return the new user




