import user, caesar, os, getpass, time
from tkinter import *
from tkinter import filedialog, scrolledtext # Imports the many modules we need

if (os.path.isfile("user.txt")): # If the user file exists,
    loadeduser = user.LoadUserFromFile() # Load the user
    while True:
        attemptusername = input("Please enter your username: ") # Ask for the username
        if attemptusername != loadeduser.username: # If they don't match
            print("Incorrect username!")
            continue # Make them try again
        attemptedpassword = getpass.getpass("\nPlease enter your password: ") # Prompt the user for their password
        if attemptedpassword != loadeduser.password: # If the password is wrong
            time.sleep(1) # Start a cooldown (to ratelimit)
            print("Incorrect password!")
            continue # Make them try again
        else:
            print("Login successful!") # Otherwise end the loop
            break
else: # If there is no user file
    print("The following will create an account with which you can authenticate your entry.\nDon't forget your credentials otherwise you will be locked out.")
    # Prompt the user
    username = input("Enter a username: ") # Ask for a username
    user.User(username) # Initialise a new user class to walk the user through the setup process.

class Application: # Defines main GUI application
    def __init__(self,master):
        self.master = master # Set Tk master
        master.title("Caesar Shift") # Set window title
        self.CreateWidgets() # Create GUI widgets

    def CreateWidgets(self):
        self.openbutton = Button(self.master,text="Open",command = self.OpenFile)
        self.savebutton = Button(self.master,text="Save As",command = self.SaveFile)
        self.quicksavebutton = Button(self.master,text="Save",command = lambda: self.SaveFile(quicksave=True)) # Create buttons for all functions
        self.quicksavebutton.configure(state=DISABLED) # Disable the quicksave button
        self.filelabel = Label(self.master,text="Editing: [Untitled]") # Create a label to show the currently active docuent
        self.editor = scrolledtext.ScrolledText(self.master, width = 95, height = 33) # Create a widget to allow the user to edit text
        self.editor.configure(background="white") # Set the background to be white so that it is clear

        self.openbutton.grid(row=0,column=0,sticky="NSEW")
        self.savebutton.grid(row=0,column=1,sticky="NSEW")
        self.quicksavebutton.grid(row=0,column=2,sticky="NSEW")
        self.filelabel.grid(row=1,column=0,columnspan=3,sticky="W")
        self.editor.grid(row=2,column=0,columnspan=3,rowspan = 30) # Position all of these elements on the screen

    def SaveFile(self, quicksave=False):
        if not quicksave: # If this is a full "save as"
            self.filename = filedialog.asksaveasfilename(title = "Select File",filetypes = (("Text files","*.txt"),("All files","*.*"))) # Prompt the user for a file
        with open(self.filename,"w") as f: # Open their file
            f.write("This file is encrypted. Please open it in the software in which it was created to edit it.\n") # Write a message for people opening it in other text editors
            plaintextfile = self.editor.get(0.0,END) # Read the contents of the editing area
            encryptedfile = caesar.Shift(plaintextfile) # Shift the contents
            f.write(encryptedfile) # Write them to the file
            self.filelabel.configure(text = "Editing: " + self.filename) # Update the file label
            self.quicksavebutton.configure(state=NORMAL) # Allow quicksaving

    def OpenFile(self):
        self.filename = filedialog.askopenfilename(title = "Select File",filetypes = (("Text files","*.txt"),("All files","*.*"))) # Prompt the user for a file
        with open(self.filename, "r") as f: # Open it
            f.readline() # Remove the warning message
            encryptedfile = f.read() # Read the file 
            plaintextfile = caesar.UnShift(encryptedfile) # Unshift the file
            self.editor.delete(0.0,END) # Delete the contents of the editor
            self.editor.insert(0.0,plaintextfile) # Insert the new text
            self.filelabel.configure(text = "Editing: " + self.filename) # Update the file label
            self.quicksavebutton.configure(state=NORMAL) # Allow quicksaving


root = Tk() # Instantiates Tkinter
app = Application(root) # Instantiates the application
root.mainloop() # Starts the application mainloop
